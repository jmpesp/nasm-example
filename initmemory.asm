section .data
str:	db "Hello world",10,0 ;c style string
length:	dd $ - str - 1

section .text
    extern printf
    global main

main:
    push ebp
    mov ebp,esp

    push str 
    call printf
	add esp,4

 	lea edi,[str]

	mov ecx,dword [length]
	mov al,'A'
	rep es stosb

    push str 
    call printf
	add esp,4
    
    mov esp,ebp
    pop ebp
    ret

all: hello gcd stringlength readarray initmemory fun

hello: hello.o
	gcc -m32 -o bin/$@ $^
hello.o: hello.asm
	nasm -felf -o $@ $^

gcd: gcd.o
	gcc -m32 -o bin/$@ $^
gcd.o: gcd.asm
	nasm -felf -o $@ $^

stringlength: stringlength.o
	gcc -m32 -o bin/$@ $^
stringlength.o: stringlength.asm
	nasm -felf -o $@ $^

readarray: readarray.o
	gcc -m32 -o bin/$@ $^
readarray.o: readarray.asm
	nasm -felf -o $@ $^

initmemory: initmemory.o
	gcc -m32 -o bin/$@ $^
initmemory.o: initmemory.asm
	nasm -felf -o $@ $^

fun: fun.o
	gcc -m32 -o bin/$@ $^
fun.o: fun.asm
	nasm -felf -o $@ $^

clean:
	$(RM) *.o
	$(RM) bin/*
